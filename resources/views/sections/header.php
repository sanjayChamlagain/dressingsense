<!DOCTYPE html>
<html>
    <head>
        <title>Store</title>
        <!--/tags -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Elite Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
            function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--//tags -->
        <link href="<?php echo asset('css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo asset('css/style.css'); ?>" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo asset('css/font-awesome.css'); ?>" rel="stylesheet"> 
        <link href="<?php echo asset('css/easy-responsive-tabs.css'); ?>" rel='stylesheet' type='text/css'/>
        <link rel="stylesheet" href="<?php echo asset('css/landingpage.css');?>">
        <link rel="stylesheet" href="<?php echo asset('css/regform.css');?>">
      
        <!-- //for bootstrap working -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
         <script type="text/javascript">
      var baseUrl="<?php echo url('/');?>";
          var notification;
          <?php
          //echo $msg;
          if(Session::has('msg')):?>
              notification = JSON.parse('<?php echo Session::get('msg');?>');
          <?php endif;?>
      </script>
    </head>
    <body>
      <!-- //header -->
        <!-- header-bot -->
        <div class="header-bot">
            <div class="header-bot_inner_wthreeinfo_header_mid">
                <div class="col-md-4 header-middle">
                    <div class="header" id="home">
                        
                           
                            <ul>                                
                                <li><i class="fa fa-phone" aria-hidden="true"></i> Call : 9801832999</li>
                                <li style="width: 54%;"><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@example.com">quitthegame3@gmail.com</a></li>
                            </ul>
                    </div>
                </div>
                <!-- header-bot -->
                <div class="col-md-4 logo_agile">
                    <h1><a href="index.html"><span>D</span>ressingSense <i  class="fa fa-shopping-bag top_logo_agile_bag" aria-hidden="true" style="top: 62px;
    right: 145px;"></i></a></h1>
                </div>
                <!-- header-bot -->
                <div class="col-md-4 agileits-social top_content text-center">
                    <ul class="social-nav model-3d-0 footer-social w3_agile_social">
<!--                        <li class="share">Share On : </li>-->
                        <li><a href="#" class="facebook">
                                <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
                        <li><a href="#" class="twitter"> 
                                <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
                        <li ><a href="#" class="instagram">
                                <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
                                <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
                                           <li data-toggle="modal" data-target="#myModal2"><a href="#"  style="width: 63px;
    height: 29px;
    position: absolute;
    top: 17px;
    right: 12px;
    background-color:  #00acee;
    color: white;">Register
    <div class="front"></div>
    <div class="back"></div>
    </a></li>
     <li data-toggle="modal" data-target="#myModal1" ><a href="#"   style="width: 63px;
    height: 29px;
    position: absolute;
    top: 17px;
    right: 84px;
    background-color: #bd081c;
    color: white;">Sign In
    <div class="front"></div>
    <div class="back"></div>
    </a></li>
                    </ul>



                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- //header-bot -->
        <div id="body">